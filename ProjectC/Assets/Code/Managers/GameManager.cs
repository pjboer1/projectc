﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    private static Manager[] managers;

    public Player player;


    public static GameManager instance { get; private set; }

    private static bool inGame;
    private static bool pause;
    private bool loadLevelOnce;

    private readonly Timer loadingTimer;


    public static T GetManager<T>() where T : Manager
    {
        for (int i = 0; i < managers.Length; i++)
        {
            if (typeof(T) == managers[i].GetType())
            {
                return (T)managers[i];
            }
        }
        return default(T);
    }


    GameManager()
    {
        instance = this;
        managers = new Manager[]
        {
            new AudioManager(),
            new InputManager(),
            new AchievementManager(),
            new ScoreManager(),
            new InventoryManager(),
            new TempUIManager(),
        };


        loadingTimer = new Timer();
        loadLevelOnce = false;
    }
    
    private void Awake()
    {
        DontDestroyOnLoad(gameObject);

        for (int i = 0; i < managers.Length; i++)
        {
            managers[i].Awake();
        }
    }

    // Start is called before the first frame update
    private void Start()
    {
        loadingTimer.SetTimer();

        for (int i = 0; i < managers.Length; i++)
        {
            managers[i].Start();
        }
    }

    // Update is called once per frame
    void Update()
    {

        for (int i = 0; i < managers.Length; i++)
        {
            managers[i].Update();
        }
        if (loadingTimer.TimerDone())
        {
            loadingTimer.StopTimer();
        }

        if (!loadingTimer.isActive && !loadLevelOnce)
        {
            LoadLevel(Levels.MainMenu);
            loadLevelOnce = true;
            GetManager<AudioManager>().PlayMusic("MenuMusic", 1);
        }
    }
    public static void LoadLevel(Levels level)
    {
        SceneManager.LoadScene((int)level);

        if (level == Levels.InGameDev)
        {
            inGame = true;
            pause = false;
        }
        else
        {
            inGame = false;
        }
    }

    public static bool GetInGame()
    {
        return inGame;
    }
    public static void Pause(bool value)
    {
        pause = value;

        for (int i = 0; i < managers.Length; i++)
        {
            managers[i].Pause(value);
        }
    }
    public void ResetGame()
    {
        for (int m = 0; m < managers.Length; m++)
        {
           // managers[i].ResetGame();
        }
    }


}
public enum Levels
{
    Loading,
    MainMenu,
    InGameDev,
    InGameArt
}
