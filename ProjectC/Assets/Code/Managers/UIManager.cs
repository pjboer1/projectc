﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{

    public Canvas[] canvas;

    public void Play(int level)
    {
        GameManager.LoadLevel((Levels)level);
        GameManager.GetManager<AudioManager>().StopPlaying();
    }

    // Start is called before the first frame update
    public void Start()
    {
        foreach (var item in canvas)
        {
            item.enabled = false;
        }
    }


    // Update is called once per frame
    public void Update()
    {

    }
    public void PressQuit()
    {
        var quitCanvas = canvas[1];
        quitCanvas.enabled = true;
    }
    public void QuitYes()
    {
        Application.Quit();
    }
    public void QuitNo()
    {
        var quitCanvas = canvas[1];
        quitCanvas.enabled = false;
    }
    public void PressSettings()
    {
        foreach (var item in canvas)
        {
            item.enabled = false;
        }
        var settings = canvas[2];
        settings.enabled = true;
    }
    public void PressPlay()
    {
        foreach (var item in canvas)
        {
            item.enabled = false;
        }
        var play = canvas[0];
        play.enabled = true;
    }
    public void Return()
    {
        foreach (var item in canvas)
        {
            item.enabled = false;
        }
    }
    public void PressShop()
    {
        foreach (var item in canvas)
        {
            item.enabled = false;
        }
        var shop = canvas[3];
        shop.enabled = true;
    }

}
