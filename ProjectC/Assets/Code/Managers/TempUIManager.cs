using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TempUIManager : Manager
{
    
    // Start is called before the first frame update
    public override void Start()
    {
       
    }

    // Update is called once per frame
    public override void Update()
    {
        if (UIManagerInGame.instance == null)
        {
            return;
        }

    }

    public void CreateUI(string _text, float duration = 3)
    {
        //Parent is the panel
        Transform canvas = UIManagerInGame.instance.tempCanvas.transform;
        GameObject panel = new GameObject();
        panel.name = "TempPanel";
        panel.AddComponent<CanvasRenderer>();
        panel.AddComponent<Image>();
        panel.GetComponent<Image>().color = Color.blue;
        panel.transform.SetParent(canvas);
        var posX = panel.GetComponent<RectTransform>().position.x;
        var posY = panel.GetComponent<RectTransform>().position.y;
        var width = panel.GetComponent<RectTransform>().rect.width;
        var height = panel.GetComponent<RectTransform>().rect.height;
        posX = -5;
        posY = -236;
        width = 160;
        height = 35;
        GameObject uiSource = new GameObject();
        uiSource.transform.SetParent(panel.transform);
        Text text = uiSource.AddComponent<Text>();
        text.text = _text;
        uiSource.name = "RayText";
        Object.Destroy(uiSource, duration);
        Object.Destroy(panel, duration);

    }
}
