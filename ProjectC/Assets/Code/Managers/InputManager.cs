﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : Manager
{
    private ControlScheme[] schemes;


    public InputManager()
    {
        schemes = new ControlScheme[]
        {
            new ControlScheme()
            {
                _input = new InputBinding[]
                {
                new InputBinding(){ mouseButton = 0 },
                new InputBinding() { keyCode = KeyCode.W, strokeType = KeyStrokeType.down},
                new InputBinding() { keyCode = KeyCode.S, strokeType = KeyStrokeType.down},
                new InputBinding() {keyCode = KeyCode.LeftShift, strokeType = KeyStrokeType.down },
                new InputBinding() {keyCode = KeyCode.LeftShift, strokeType = KeyStrokeType.up },
                new InputBinding() {keyCode = KeyCode.Escape, strokeType = KeyStrokeType.down },
                new InputBinding() { keyCode = KeyCode.E, strokeType = KeyStrokeType.down },
                new InputBinding() { mouseButton = 1, strokeType = KeyStrokeType.any},
                new InputBinding() { mouseButton = 1, strokeType = KeyStrokeType.up},
                new InputBinding() { keyCode = KeyCode.Tab, strokeType = KeyStrokeType.any},
                new InputBinding() { keyCode = KeyCode.Tab , strokeType = KeyStrokeType.up},
                new InputBinding() { keyCode = KeyCode.F , strokeType = KeyStrokeType.down}
                }

            }
        };
     }

    public override void Update()
    {
        if (!GameManager.GetInGame())
        {
            return;
        }
        base.Update();

        for (int i = 0; i < schemes.Length; i++)
        {
            if (schemes[i].isActive)
            {
                continue;
            }
            schemes[i].isActive = true;
            Player player = Object.Instantiate(GameManager.instance.player, new Vector3(412, 1, 132), Quaternion.identity) as Player;
            player.name = GameManager.instance.player.name = "Player: " + (i + 1);
            player.GetComponent<Player>().SetControlScheme(schemes[i]);
            if (GameManager.GetInGame())
            {
                GameManager.Pause(false);
            }
        }

    }
    public override void Pause(bool pause)
    {
        base.Pause(pause);
    }
}
