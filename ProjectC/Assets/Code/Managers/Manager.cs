﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Manager
{
    public virtual void Awake() { }
    public virtual void Pause(bool pause)
    {
        if (pause)
        {
            Time.timeScale = 0F;
            Debug.Log("I AM PAUSED");
        }
        else
        {
            Time.timeScale = 1F;
        }
    }
    public virtual void Start() { }
    public virtual void Update() { }
}
        
