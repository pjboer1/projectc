﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{

    public static SpawnManager instance { get; private set; }



    private Timer time = new Timer();

    public GameObject[] spawns;

    private GameObject enemy;

    private int randomSpawnNumber;


    public int round { get; private set; }
    private float maxNumber = 4;
    private int currentNumber = 0;
    private int killCount = 0;


    public SpawnManager()
    {
        instance = this;
        round = 0;
    }


    public void Awake()
    {
        enemy = Resources.Load<GameObject>("Prefabs/Actors/Zombie");
    }
    // Start is called before the first frame update
    public void Start()
    {

        
    }

    // Update is called once per frame
    public void Update()
    {
        if (time.isActive && time.TimerDone())
        {
            time.StopTimer();
        }

        Debug.LogWarning("CurrentNumber:  " + currentNumber + " | " + "Time left: " + time.TimeLeft());
        if (currentNumber <= 0)
        {
            ChangeRound();
        }
    }
    

    public void Spawning()
    {
        for (int i = 0; i < currentNumber; i++)
        {
            var random = Random.Range(0, spawns.Length);
            SpawningObject(enemy, 1, spawns[random]);
        }
    }


    public void SpawningObject(GameObject _entity, float delay, GameObject spawnpoint)
    {
           Instantiate(_entity, new Vector3(spawnpoint.transform.position.x, spawnpoint.transform.position.y, spawnpoint.transform.position.z), Quaternion.identity);
    }
    public void ChangeRound()
    {
        if (round >= 10)
        {
            maxNumber = maxNumber * 1.2f * 1.05f;
        }
        else
        {
            maxNumber = maxNumber * 1.2f;
        }
        
        currentNumber = Mathf.RoundToInt(maxNumber);
        Invoke("Spawning", 5f);
        round++;
        UIManagerInGame.instance.UpdateUI(UIManagerInGame.instance.round, "Round: " + round.ToString());
        
      //  GameManager.GetManager<AudioManager>().PlaySound("roundChangeSound");
    }



    public int SubstractNumber(int amount)
    {
        killCount += 1;
        return currentNumber -= amount;
    }

    public int GetKillCount()
    {
        return killCount;
    }
}
