﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuyManager : MonoBehaviour
{
    Player player = Player.instance;

    public enum Type
    {
        Perk,
        Ammo,
        MysteryBox,
        Door,
        Wallbuy,
        Pack
    }

    public enum PerkType
    {
        Juggernaut,
        Speedcola,
        StaminaUp,
        Doubletap,
        NONE,
    }
    public string buyName;
    public Type buyType;
    public PerkType perkType;
    public int cost;
    public Canvas buyUI;
    public Text buyText;
    public string description;


    // Start is called before the first frame update
    public  void Start()
    {
        
    }

    // Update is called once per frame
    public  void Update()
    {
        
    }
    
    public void Interact()
    {
        if (player.GetPoints() < cost)
        {
            UIManagerInGame.instance.ShowTempUI(UIManagerInGame.instance.RayCanvas, "You don't have enough points to buy this!");
            return;
        }
        switch (buyType)
        {
            case Type.Perk:
                break;
            case Type.Ammo:
                    player.weapon.weaponData.currentAmmo = player.weapon.weaponData.maxAmmo;
                    player.weapon.weaponData.bulletsleft = player.weapon.weaponData.magazineSize;
                    player.LosePoints(cost);
                    UIManagerInGame.instance.UpdateUI(UIManagerInGame.instance.ammo, " " + player.weapon.weaponData.bulletsleft + " | " + player.weapon.weaponData.currentAmmo);
                break;
            case Type.MysteryBox:
                break;
            case Type.Door:
                break;
            case Type.Wallbuy:
                break;
            default:
                break;
        }
    }
}

