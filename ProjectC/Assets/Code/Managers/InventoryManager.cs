﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryManager : Manager
{
    public GameObject[] weapons;

    public int points { get; private set; }

    public int selectedSlot;

    private GameObject currentWeapon;

    public GameObject primary;
    public GameObject secondary;

    public GameObject equipment;
    

    public InventoryManager()
    {
        weapons = new GameObject[2];
    }

    // Start is called before the first frame update
    public override void Start()
    {
        currentWeapon = Player.instance.currentweapon;
    }

    // Update is called once per frame
    public override void Update()
    {

    }

    public void SwapWeapon()
    {

    }

    public void ManageItems()
    {

    }
    public override void Pause(bool pause)
    {
        base.Pause(pause);
    }
}
