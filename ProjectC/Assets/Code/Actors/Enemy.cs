﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : Actor
{

    private NavMeshAgent navAgent;
   
    private int hitPoints;
    private int diePoints;

    public static Enemy instance {get; private set;}

    public int deathAnimationTimer = 2;
    // Start is called before the first frame update
    public override void Start()
    {
        instance = this;
        health = 100;
        hitPoints = 10;
        diePoints = 150;
        navAgent = gameObject.GetComponent<NavMeshAgent>();
       
    }

    // Update is called once per frame
    public override void Update()
    {
        if (health <= 0)
        {
            Die();
        }
        if (gameObject != null && Player.instance.gameObject != null)
        {
            navAgent.destination = Player.instance.gameObject.transform.position;
        }
      
 
    }

    public void TakeDamage(int damage)
    {
        health -= damage;
        if (health <= 0)
        {
            Player.instance.GainPoints(diePoints);
        }
        else
        {
            Player.instance.GainPoints(hitPoints);
        }
       
        print("Health: " + health + " Damage: " + damage);
    }
    public void Die()
    {
        SpawnManager.instance.SubstractNumber(1);
        Destroy(gameObject);
    }

  
}
