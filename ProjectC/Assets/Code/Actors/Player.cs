﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : Actor
{
    
    public static Player instance { get; private set; }

    bool shooting, readyToShoot, reloading;
    new bool IsInvoking;

    public Camera fpsCam;
    public Transform attackPoint;
    public RaycastHit rayHit;
    public LineRenderer bullet;
    private WaitForSeconds shotDuration = new WaitForSeconds(.07f);

    public GameObject currentweapon;

    private ControlScheme scheme;
    private BuyManager RaycastUI;



    public float speed = 6.0F;
    public float sprintSpeed = 10f;
    public float scopingSpeed = 2.0F;
    public float jumpSpeed = 2.0F;
    public float gravity = 20.0F;
    private Vector3 moveDirection = Vector3.zero;
    private float turning;
    private float looking;
    public float sensitivity;
    public int meleeDamage = 100;
    public int meleeDistance = 2;

    public bool sprintBool = false;
    private enum RotationAxes { mouseXandY = 0, mouseX = 1, mouseY = 2 }
    private RotationAxes axes = RotationAxes.mouseXandY;
    private float sensitivityX = 5f;
    private float sensitivityY = 5f;
    private float minimumY = -60F;
    private float maximumY = 60F;
    private float rotationY = 0f;

    private bool isScoping;

    public GameObject scopeLoc;
    public GameObject unscopedLoc;

    public WeaponHandler weapon;
    private InventoryManager inventory;

    private int currentPoints;
    private int startingPoints;

    private int interactDistance = 2;

    


    bool onlyonce = true;

    Player()
    {
        instance = this;
    }

    // Start is called before the first frame update
    public override void Start()
    {
        base.Start();
        inventory = GameManager.GetManager<InventoryManager>();
        inventory.selectedSlot = 0;
        weapon = currentweapon.GetComponent<WeaponHandler>();
        inventory.weapons.SetValue(currentweapon, inventory.selectedSlot);
        Cursor.lockState = CursorLockMode.Locked;
        startingPoints = 500;
        currentPoints = startingPoints;
   
    }

    // Update is called once per frame
    public override void Update()
    {

        /*   if (reloading)
           {
               weapon.anim.SetBool("reload", true);
           }
           else
           {
               weapon.anim.SetBool("reload", false);
           }

       */

        if (weapon.weaponData.currentAmmo <= 0)
        {
            weapon.weaponData.currentAmmo = 0;
        }

        if (onlyonce)
        {
           
           
            weapon.weaponData.bulletsleft = weapon.weaponData.magazineSize;
            weapon.weaponData.currentAmmo = weapon.weaponData.startingAmmo;
            UIManagerInGame.instance.UpdateUI(UIManagerInGame.instance.ammo, " " + weapon.weaponData.bulletsleft + " | " + weapon.weaponData.currentAmmo);
            UIManagerInGame.instance.UpdateUI(UIManagerInGame.instance.health, "HP: " + health);
            UIManagerInGame.instance.UpdateUI(UIManagerInGame.instance.points, currentPoints.ToString());
            readyToShoot = true;
            onlyonce = false;
        }
        base.Update();
        scheme.Update();
        print(UIManagerInGame.instance.pauseMenu);
        if (!UIManagerInGame.instance.pauseMenu)
        {
            Movement();
            CameraMove();
            MyInput();
            InteractUI();
            TestMethod();
         
        }


    }

    public void Fire()
    {

    }

    public void SetControlScheme(ControlScheme scheme)
    {
        if (scheme != null)
        {
            ClearControlScheme();
        }
        scheme.AssignInput(this);
        this.scheme = scheme;

    }
    public void ClearControlScheme()
    {
        scheme = null;
    }

    

    public void MoveForward()
    {

    }

    public void MoveBackWards()
    {

    }

    public void Sprint()
    {
        if (!isScoping)
        {
            sprintBool = true;
        }
    
    }
    public void UnSprint()
    {
        sprintBool = false;
    }

    public void Pause()
    {
        if (!UIManagerInGame.instance.pauseMenu)
        {
            GameManager.Pause(true);
            Cursor.lockState = CursorLockMode.None;
            UIManagerInGame.instance.canvas[0].enabled = true;
            UIManagerInGame.instance.pauseMenu = true;
        }
        else
        {
            UIManagerInGame.instance.ReturnButton();
        }
      
       
    }



    private void Movement()
    {
        CharacterController controller = GetComponent<CharacterController>();

        if (controller.isGrounded)
        {

            moveDirection = new Vector3(Input.GetAxis("Horizontal") * 0.4f, 0, Input.GetAxis("Vertical"));
            moveDirection = transform.TransformDirection(moveDirection);

            if (!sprintBool && !isScoping)
            {
                moveDirection *= speed;
            }
            else if (sprintBool && !isScoping)
            {
                moveDirection *= sprintSpeed;
            }
            else if (isScoping)
            {
                moveDirection *= scopingSpeed;
            }

            if (Input.GetButton("Jump"))
                moveDirection.y = jumpSpeed;

        }
        turning = Input.GetAxis("Mouse X") * sensitivity;
        looking = -Input.GetAxis("Mouse Y") * sensitivity;
        if (turning != 0)
        {

            transform.localEulerAngles += new Vector3(0, turning, 0);
        }
        if (looking != 0)
        {

            transform.localEulerAngles += new Vector3(looking, 0, 0);
        }

        moveDirection.y -= gravity * Time.deltaTime;

        controller.Move(moveDirection * Time.deltaTime);
    }
    private void CameraMove()
    {
        if (axes == RotationAxes.mouseXandY)
        {
            float rotationX = transform.localEulerAngles.y + Input.GetAxis("Mouse X") * sensitivityX;

            rotationY += Input.GetAxis("Mouse Y") * sensitivityY;
            rotationY = Mathf.Clamp(rotationY, minimumY, maximumY);

            transform.localEulerAngles = new Vector3(-rotationY, rotationX, 0);


        }
        else if (axes == RotationAxes.mouseX)
        {
            transform.Rotate(0, Input.GetAxis("Mouse X") * sensitivityX, 0);
        }
        else
        {
            rotationY += Input.GetAxis("Mouse Y") * sensitivityY;
            rotationY = Mathf.Clamp(rotationY, minimumY, maximumY);

            transform.localEulerAngles = new Vector3(-rotationY, transform.localEulerAngles.y, 0);
        }
    }

    public void FireWeapon()
    {
        readyToShoot = false;

        float x = UnityEngine.Random.Range(-weapon.weaponData.spread, weapon.weaponData.spread);
        float y = UnityEngine.Random.Range(-weapon.weaponData.spread, weapon.weaponData.spread);

        Vector3 direction = fpsCam.transform.forward + new Vector3(x, y, 0);

        Vector3 rayOrigin = fpsCam.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, 0));
        bullet.SetPosition(0, attackPoint.position);

        if (Physics.Raycast(rayOrigin, direction, out rayHit, weapon.weaponData.range))
        {
            bullet.SetPosition(1, rayHit.point);
            StartCoroutine(shotEffect());
            Debug.Log(rayHit.collider.name);
            Debug.DrawRay(fpsCam.transform.position, direction, Color.red, 100, true);

            if (rayHit.collider.CompareTag("Enemy"))
            {
                rayHit.collider.GetComponent<Enemy>().TakeDamage(weapon.weaponData.damage);
            }
        }
        else
        {
            bullet.SetPosition(1, rayOrigin + (fpsCam.transform.forward) * weapon.weaponData.range);
        }

        weapon.weaponData.bulletsleft--;
        weapon.weaponData.bulletsShot--;

        UIManagerInGame.instance.UpdateUI(UIManagerInGame.instance.ammo, " " + weapon.weaponData.bulletsleft + " | " + weapon.weaponData.currentAmmo); 

        print(weapon.weaponData.bulletsleft + " | " + weapon.weaponData.bulletsShot);


        if (!IsInvoking("ResetShot") && !readyToShoot)
        {
            Invoke("ResetShot", weapon.weaponData.timeBetweenShooting);
        }
        if (weapon.weaponData.bulletsShot > 0 && weapon.weaponData.bulletsleft > 0)
        {
            Invoke("FireWeapon", weapon.weaponData.timeBetweenShots);
        }
    }

    private IEnumerator shotEffect()
    {
        //Play Audio
        bullet.enabled = true;
        yield return shotDuration;
        bullet.enabled = false;
    }

    private void MyInput()
    {
       // print(readyToShoot + " | " + shooting + " | " + weapon.bulletsleft + " | " + reloading + " | " + weapon.magazineSize);
        if (weapon.weaponData.allowButtonHold)
        {
            shooting = Input.GetKey(KeyCode.Mouse0);
        }
        else
        {
            shooting = Input.GetKeyDown(KeyCode.Mouse0);
        }
        if (Input.GetKeyDown(KeyCode.R) && weapon.weaponData.bulletsleft < weapon.weaponData.magazineSize && !reloading)
        {
            Reload();
        }

        if (readyToShoot && shooting && !reloading && weapon.weaponData.bulletsleft > 0)
        {

            weapon.weaponData.bulletsShot = weapon.weaponData.bulletsPerTap;
            FireWeapon();
            print("Fireeee");
        }
    }

    private void ResetShot()
    {
        readyToShoot = true;
    }

    private void Reload()
    {
        reloading = true;
        Invoke("ReloadFinished", weapon.weaponData.reloadTime);
    }

    private void ReloadFinished()
    {

        if (weapon.weaponData.currentAmmo <= 0 && weapon.weaponData.bulletsleft <= 0)
        {
           // UIManagerInGame.instance.ShowTempUI(UIManagerInGame.instance.tempCanvas, "You are out of ammo!");
        }
        else
        {
            if (weapon.weaponData.currentAmmo <= weapon.weaponData.magazineSize)
            {
                weapon.weaponData.bulletsleft = weapon.weaponData.currentAmmo;
                weapon.weaponData.currentAmmo = 0;
            }
            else
            {
                weapon.weaponData.bulletsleft = weapon.weaponData.magazineSize;
            }
            weapon.weaponData.currentAmmo -= weapon.weaponData.magazineSize;
        }
       
        UIManagerInGame.instance.UpdateUI(UIManagerInGame.instance.ammo, " " + weapon.weaponData.bulletsleft + " | " + weapon.weaponData.currentAmmo);
        reloading = false;
    }

    public void Scope()
    {
         currentweapon.transform.position = Vector3.Slerp(unscopedLoc.transform.position, scopeLoc.transform.position, 3);
         fpsCam.fieldOfView = 50;
         isScoping = true;
    }
    public void UnScope()
    {
        currentweapon.transform.position = Vector3.Slerp(scopeLoc.transform.position, unscopedLoc.transform.position, 3); 
        fpsCam.fieldOfView = 60;
        isScoping = false;
    }


    public void GainPoints(int _number)
    {
        currentPoints += _number;
        UIManagerInGame.instance.UpdateUI(UIManagerInGame.instance.points, currentPoints.ToString());
    }

    public void LosePoints(int _number)
    {
        currentPoints -= _number;
        UIManagerInGame.instance.UpdateUI(UIManagerInGame.instance.points, currentPoints.ToString());
    }
    public int GetPoints()
    {
        return currentPoints;
    }


    public void Melee()
    {
        Vector3 rayOrigin = fpsCam.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, 0));
        if (Physics.Raycast(rayOrigin, fpsCam.transform.TransformDirection(fpsCam.transform.forward), out rayHit, meleeDistance))
        {
            if (rayHit.collider.CompareTag("Enemy"))
            {
                Debug.DrawRay(gameObject.transform.position, gameObject.transform.forward, Color.red, 100, true);
                rayHit.collider.GetComponent<Enemy>().TakeDamage(meleeDamage);
                print("Used Melee");
            }
        }
    }
    public void Scoreboard()
    {
        UIManagerInGame.instance.scoreBoard.gameObject.SetActive(true);
        UpdateScoreboard(UIManagerInGame.instance.scoreboardText[0], GameManager.instance.player.name);
        UpdateScoreboard(UIManagerInGame.instance.scoreboardText[1], GetPoints().ToString());
        UpdateScoreboard(UIManagerInGame.instance.scoreboardText[2], SpawnManager.instance.round.ToString());
        UpdateScoreboard(UIManagerInGame.instance.scoreboardText[3], "Scoreboard");
        UpdateScoreboard(UIManagerInGame.instance.scoreboardText[4], SpawnManager.instance.GetKillCount().ToString());
    }
    public void ScoreboardUp()
    {
        UIManagerInGame.instance.scoreBoard.gameObject.SetActive(false);
    }

    public Text UpdateScoreboard(Text _text, string _string)
    {
        _text.text = _string;
        return _text;
    }

    public void Interact()
    {
        Vector3 rayOrigin = fpsCam.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, 0));
        if (Physics.Raycast(rayOrigin, fpsCam.transform.TransformDirection(transform.forward), out rayHit, interactDistance))
        {
            if (rayHit.collider.CompareTag("Buy"))
            {
               var buy = rayHit.collider.gameObject.GetComponent<BuyManager>();

                buy.Interact();
            }
        }
    }
    public void InteractUI()
    {
        Vector3 rayOrigin = fpsCam.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, 0));
        if (Physics.Raycast(rayOrigin, fpsCam.transform.TransformDirection(transform.forward), out rayHit, interactDistance))
        {
            if (rayHit.collider.CompareTag("Buy"))
            {
                RaycastUI = rayHit.collider.gameObject.GetComponent<BuyManager>();
                RaycastUI.buyUI.gameObject.SetActive(true);
                RaycastUI.buyText.text = string.Format("{0} \n Cost: {1} \n {2}", RaycastUI.buyName, RaycastUI.cost, RaycastUI.description);
            }
        }
        else
        {
            if (RaycastUI.isActiveAndEnabled)
            {
                RaycastUI.buyUI.gameObject.SetActive(false);
            }
            
        }

    }
    public void DisableUI()
    {
        RaycastUI.buyUI.gameObject.SetActive(false);
    }

    public Vector3 GetLoc(string posX, string posY, string posZ)
    {
        Vector3 playerLoc = gameObject.transform.position;

        playerLoc.x = PlayerPrefs.GetFloat(posX);
        playerLoc.y = PlayerPrefs.GetFloat(posY);
        playerLoc.z = PlayerPrefs.GetFloat(posZ);

        return playerLoc;
    }

    public void TestMethod()
    {
        if (Input.GetKeyDown(KeyCode.L))
        {
            print("Test L");
            GameManager.GetManager<TempUIManager>().CreateUI("Test");
        }
    }
}
