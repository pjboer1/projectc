﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Actor : MonoBehaviour
{

    public float health;

    public virtual void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }
    // Start is called before the first frame update
   public virtual void Start()
    {
        
    }

    // Update is called once per frame
   public virtual void Update()
    {
        
    }
}
