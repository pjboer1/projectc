﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Weapons", menuName = "WeaponCreator", order = 1)]
public class Weapon : ScriptableObject
{
    public string weaponName;
    public int ID;
    public int damage;
    public float timeBetweenShooting, spread, range, reloadTime, timeBetweenShots, bulletSpeed;
    public int magazineSize, bulletsPerTap;
    public bool allowButtonHold;
    public int bulletsleft, bulletsShot;
    public int maxAmmo;
    public int currentAmmo;
    public int startingAmmo;
    public Type weaponType;
    public ReloadType reloadType;
    public Animator anim;
    public Material shader;
    public Mesh weaponMesh;
    

    Weapon()
    {

    }
    public void Start()
    {
        if (GameManager.GetManager<InventoryManager>().weapons.GetValue(0) != null)
        {
            Debug.LogFormat(" " + GameManager.GetManager<InventoryManager>().weapons.GetValue(0).ToString());
        }
        else
        {
            return;
        }
        
    }


    public enum Type
    {
        AssaultRifle,
        SubMachineGun,
        TacticalRifle,
        LightMachineGun,
        SniperRifle,
        Shotgun,
        Pistol,
        Special,
        Melee,
        Launcher,
    }
    public enum ReloadType
    {
        Magazine,
        Bolt,
        Lever,
        Revolver,
        Pump
    }
}

