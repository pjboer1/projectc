﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlScheme
{
    public bool isActive;
    public Dictionary<InputAction, InputBinding> input;
    public InputBinding[] _input;

    public ControlScheme()
    {
        input = new Dictionary<InputAction, InputBinding>();
        _input = new InputBinding[System.Enum.GetNames(typeof(InputAction)).Length];
    }

    public void Update()
    {
        if (!isActive || input == null || _input.Length == 0)
        {
            return;
        }

        foreach (InputBinding binding in _input)
        {
            binding.HandleBinding();
        }
    }

    public void AssignInput(Player player)
    {
        _input[(int)InputAction.Fire].action = player.Fire;
        _input[(int)InputAction.MoveForward].action = player.MoveForward;
        _input[(int)InputAction.MoveBackwards].action = player.MoveBackWards;
        _input[(int)InputAction.Sprint].action = player.Sprint;
        _input[(int)InputAction.UnSprint].action = player.UnSprint;
        _input[(int)InputAction.Pause].action = player.Pause;
        _input[(int)InputAction.Melee].action = player.Melee;
        _input[(int)InputAction.Scope].action = player.Scope;
        _input[(int)InputAction.UnScope].action = player.UnScope;
        _input[(int)InputAction.Scoreboard].action = player.Scoreboard;
        _input[(int)InputAction.ScoreboardUp].action = player.ScoreboardUp;
        _input[(int)InputAction.Interact].action = player.Interact;
    }




}
