﻿public enum InputAction
{
    Fire,
    MoveForward,
    MoveBackwards,
    Sprint,
    UnSprint,
    Pause,
    Melee,
    Scope,
    UnScope,
    Scoreboard,
    ScoreboardUp,
    Interact,
}