﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManagerInGame : MonoBehaviour
{
    public static UIManagerInGame instance { get; private set; }
    public Text ammo;
    public Text health;
    public Text round;
    public Text points;
    public Canvas RayCanvas;
    public Canvas tempCanvas;


    public Canvas scoreBoard;
    public Text[] scoreboardText;

    public Timer timer = new Timer();

    public Canvas[] canvas;

    [HideInInspector]
    public bool pauseMenu = false;

    // Start is called before the first frame update
    void Start()
    {
        round.text = "Round: 1";
        instance = this;

        for (int i = 0; i < canvas.Length; i++)
        {
            canvas[i].enabled = false;
        }
        scoreBoard.gameObject.SetActive(false);
    }


    // Update is called once per frame
    void Update()
    {
        if (timer.TimerDone() && timer.isActive)
        {
            timer.StopTimer();
          
        }
    }

    public void UpdateUI(Text text, string _string)
    {
        text.text = _string;
    }

    public void ShowTempUI(Canvas _canvas,  string _string)
    {
        _canvas.gameObject.SetActive(true);
        _canvas.gameObject.GetComponentInChildren<Text>().text = _string;
        timer.SetTimer(3);
       
    }

    public void DisableUI(Canvas _canvas)
    {
        _canvas.gameObject.SetActive(false);
    }


    public void Quit()
    {

        //GameManager.instance.ResetGame();
        GameManager.LoadLevel(Levels.MainMenu);

    }
    public void QuitDesk()
    {
        Application.Quit();
    }
    public void ReturnButton()
    {
        for (int i = 0; i < canvas.Length; i++)
        {
            canvas[i].enabled = false;
        }
        GameManager.Pause(false);
        pauseMenu = false;
        Cursor.lockState = CursorLockMode.Locked;
    }
    public void Settings()
    {
        var settings = canvas[3].enabled = true;
    }
    public void QuitMainButton()
    {
        var quitmain = canvas[1].enabled = true;
    }

    public void QuitDeskButton()
    {
        var quitdesk = canvas[2].enabled = true;

    }
    public void Return()
    {

        for (int i = 1; i < canvas.Length; i++)
        {
            canvas[i].enabled = false;
        }
    }
}
